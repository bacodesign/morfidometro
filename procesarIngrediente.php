<?php

## Agregar ingrediente
#Isset comprueba que las variables no estén vacías; resuelvo con el operador ternario (condicion ? verdadero :  falso)
$agr = isset($_POST['agr']) ? $_POST['agr'] : null;
$can = isset($_POST['can']) ? $_POST['can'] : null;
$cos = isset($_POST['cos']) ? $_POST['cos'] : null;
$per = isset($_POST['pper']) ? $_POST['pper'] : null;
$id_comida = isset($_POST['id_comida']) ? $_POST['id_comida'] : null;

if (isset($agr, $can, $cos, $per, $id_comida)) {
    try {
        include 'database/conexion.php';

        $cnx = new PDO(DB_INFO, DB_USER, DB_PASS);

        $query = "INSERT INTO agregado(agregados, cant, costos, ppersona, comida_id) 
        VALUES (:agregados, :cantidad, :costo, :per, :id_comida)";
        
        $parameters = array(
            'agregados' => $agr,
            'cantidad' => $can,
            'costo' => $costo,
            'per' => $per,
            'id_comida' => $id_comida,
        );

        $consulta = $cnx->prepare($query);

        $result = $consulta->execute($parameters);

        if ($result) {
            header("Location: agregarIngrediente.php?result=ok");
        }

    } catch (ExceptionType $e) {
        //⋮ handle the exception
        'Unable to connect to the database server: ' . $e;
    }
}