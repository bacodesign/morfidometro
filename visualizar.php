<!doctype html>


<?php if (!file_exists('database/conexion.php')) die('El archivo de conexion no existe'); ?>
<title>Publicaciones</title>
</head>
<body>

<!--el head-->
<?php include 'parciales/_head.php'; ?>
<!--fin del head-->

<body class="nude docs">

<!--barra de navegacion-->
<?php include 'parciales/nav.php' ?>
<!--fin barra nav-->

<div class="wrapper">

    <div class="main">
        <div class="section section-white">
            <div class="container">

                <div class="card">
                    <div class="header">
                        <h4 class="title">
                            Agregar menú
                        </h4>
                    </div>
                    <div class="content">

                        <!--    <form action="morfi.php" method="post">-->
                        <!--        <input value="volver" type="submit" />-->
                        <!--    </form>-->
                        <!--    <form action="visualizar2.php" method="post">-->
                        <!--        <input value="proximo" type="submit" />-->
                        <!--    </form>-->

                        <div class="content table-responsive table-full-width">
                            <table class="table table-striped">

                                <?php
                                include 'database/conexion.php';
                                try {

                                    $pdo = new PDO(DB_INFO, DB_USER, DB_PASS);

                                    # Para que genere excepciones a la hora de reportar errores.
                                    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);


                                    $consulta = "SELECT * FROM comida";
                                    $preparo = $pdo->prepare($consulta);

                                    $resultado = $preparo->execute();
                                    $preparo->fetch(PDO::FETCH_ASSOC);

                                } catch (Exception $e) {
                                    //⋮ handle the exception
                                    //echo "hubo un error de cnx";
                                }

                                if (isset($resultado)) {
                                    ?>
                                    <thead>
                                    <th>Nombre</th>
                                    <th>acciones</th>
                                    </thead>
                                    <?php
                                    while ($row = $preparo->fetch(PDO::FETCH_ASSOC)) {
                                        ?>
                                        <tr>
                                            <td><?php echo $row['comida2']; ?></td>
                                            <td>
                                                <a href="visualizar2.php/?comida=<?php echo $row['id']; ?>">Ver mas</a>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                } else {
                                ?>
                                <tbody>

                                <tr>
                                    <td colspan="2">No hay resultados</td>
                                </tr>
                                <?php
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- el footer de la página -->
                        <?php include 'parciales/_footer.php'; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>