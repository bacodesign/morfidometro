<!doctype html>
<?php
// Si no existe el archivo para la conexion no muestra nada.
if (!file_exists('database/conexion.php')) die('El archivo de conexion no existe');

?>
<!--el head-->
<?php include 'parciales/_head.php'; ?>
<!--fin del head-->

<body class="nude docs">

<!--barra de navegacion-->
<?php include 'parciales/nav.php' ?>
<!--fin barra nav-->

<div class="wrapper">

    <div class="main">
        <div class="section section-white">
            <div class="container">
                <?php include 'parciales/forms/_comida.php'; ?>

                <!-- el footer de la página -->
                <?php include 'parciales/_footer.php'; ?>
            </div>
        </div>
    </div>

</div>
</body>
<!--los archivos de js van cargados al final para que la página cargue mas rápido-->
<?php include 'parciales/_footerScripts.php'; ?>

</html>
