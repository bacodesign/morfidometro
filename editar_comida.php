<?php
if (!file_exists('database/conexion.php')) die('El archivo de conexion no existe');


if(isset($_POST['id'])) {

    try {
        include 'database/conexion.php';
        // actualizamos
        // en este caso, tenemos varios campos, así que por seguridad
        // es mejor etiquetarlos antes que pasar signos de interrogación


        $cnx = new PDO(DB_INFO, DB_USER, DB_PASS);
        $cnx->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $name = htmlspecialchars(strip_tags($_POST['nombre']));
        $id = $_POST['id'];

        $query = "UPDATE comida
                    SET comida2 = :nombre
                    WHERE id = :id
                 ";

        // Prepare statement
        $stmt = $cnx->prepare($query);

        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->bindParam(':nombre', $name, PDO::PARAM_STR);

        // execute the query
        if ($stmt->execute()) {
            header('Location: /visualizar.php');

        } else {
            echo "<div class='alert alert-danger'>Unable to update record. Please try again.</div>";
        }

    } // show errors
    catch (PDOException $exception) {
        die('ERROR: ' . $exception->getMessage());
    }
}
