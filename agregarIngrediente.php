<!doctype html>


<?php
if (!file_exists('database/conexion.php')) {
    die('El archivo de conexion no existe');
}

if (!isset($_POST['com'])) {
    header('Location:index.php');

} else {
    try {
        //⋮ do something risky
        include 'database/conexion.php';

        $cnx = new PDO(DB_INFO, DB_USER, DB_PASS);

        $consulta = $cnx->prepare("INSERT INTO comida (comida2) VALUES (:comida)");

        $resultado = $consulta->execute(array(
            'comida' => $_POST['com'],
        ));

        $id_comida = $cnx->lastInsertId();

        if (isset($_POST['id_comida'])) {
            $id_comida = $_POST['id_comida'];
        }
    } catch (ExceptionType $e) {
        //⋮ handle the exception
        'Unable to connect to the database server: ' . $e;
    }

}

?>


<!--el head-->
<?php include 'parciales/_head.php';?>
<!--fin del head-->

<body class="nude docs">

<!--barra de navegacion-->
<?php include 'parciales/nav.php'?>
<!--fin barra nav-->

<!-- formulario con ingredientes -->
<?php include 'parciales/forms/_ingredientes.php';?>
<!-- fin formulario ingredientes -->

</body>
<!--los archivos de js van cargados al final para que la página cargue mas rápido-->
<?php include 'parciales/_footerScripts.php';?>

</html>
