    
<?php
if (!file_exists('database/conexion.php')) die('El archivo de conexion no existe');

/**
 * Carga el nuevo archivo de conexión
 */
include 'database/conexion.php';
?>
<html>
    <body>
         <a href="/">Home</a>

        <form action="morfi.php" method="post">
        </form>
    <body>
    <html>
        <?php
        $idComida = isset($_GET['comida']) ? $_GET['comida'] : null;

        $cnx = new PDO(DB_INFO, DB_USER, DB_PASS);
        $consulta = $cnx->prepare("SELECT * FROM comida c left join agregado a on a.comida_id = c.id  where c.id = :comida ");

        $resultado = $consulta->execute(array(
            ':comida' => $idComida
        ));

        if (!is_null($resultado)) {
            $resultado = $consulta->fetchAll();
        }
        ?>

        <table>
            <?php if ($resultado != false) { ?>
                <tr>
                    <th>Comida</th>
                    <th>Agregado</th>
                    <th>Cantidad gr</th>
                    <th>Costos en $</th>
                    <th>Personas</th> 
                    <th colspan="2">Acciones</th> 
                </tr>
                <tr>
                    <?php // print_r($resultado[0]); ?>
                    <td colspan="5"> <h3><?php echo $resultado[0]['comida2']; ?></h3> </td>
                    <td> <a href="../editar.php?id=<?php echo $resultado[0][0]; ?>&flag=m">Editar</a></td>
                    <td> <a href="../borrar.php?id=<?php echo $resultado[0][0]; ?>&flag=m">Borrar</a></td>
                </tr>
                <tr>
                    <?php foreach ($resultado as $row) {
                        ?>
                        <td></td>
                        <td> 
                            <?php echo isset($row['agregados']) ? $row['agregados'] : '-'; ?>
                        </td>
                        <td>
                            <?php echo isset($row['cant']) ? $row['cant'] : NULL; ?> gr.
                        </td>
                        <td>
                            $ <?php echo $row['costos']; ?>
                        </td>
                        <td>
                            <?php echo $row['ppersona']; ?>
                        </td>
                        <td>
                            <a href="../editar.php?id=<?php echo $row['id']; ?>&flag=i">Editar</a>
                        </td>
                        <td>
                            <a href="../borrar.php?id=<?php echo $row['id']; ?>&flag=i">Borrar</a>
                        </td>
                    </tr>
                <?php
                }
            } else {
                ?>
                <tr><td colspan="2">No hay resultados</td></tr>
            <?php } ?>

        </table>
    </body>
</html>

