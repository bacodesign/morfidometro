<div class="card">
    <div class="header">
        <h4 class="title">
            Agregar menú
        </h4>
    </div>
    <div class="content">

        <form action="../../agregarIngrediente.php" method="post">

            <div class="row">
                <div class="col-md-5">
                    <div class="form-group">
                        <label>Pedido:</label>
                        <input type="text" name="com" class="form-control border-input"/>
                    </div>
                </div>
            </div>
            <div class="text-left">
                <input type="submit" value="Agregar" class="btn btn-primary btn-fill"/>
                <a href="../visualizar.php" class="btn btn-info">  Ver </a>
            </div>
        </form>
    </div>
</div>