<div class="wrapper">

<div class="main">
    <div class="section section-white">
        <div class="container">

            <div class="card">
                <div class="header">
                    <h4 class="title">
                        Agregar menú
                    </h4>
                </div>
                <div class="content">
                    <?php 
                    if (isset($_GET['result'])) {
                        echo "agregado ok";
                    }
                    ?>
                    <form action="../../procesarIngrediente.php" method="post">

                        <div class="row">
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label>Agregado:</label>
                                    <input type="text" name="agr"/>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label>Cantidad en gr:</label>
                                    <input type="number" name="can"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label> Costo $:</label>
                                    <input type="number" name="cos"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label>Por persona:</label>
                                    <input type="number" name="pper"/><br><br>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" value="<?php echo $id_comida; ?>" name="id_comida">

                        <div class="text-left">
                            <input type="submit" name="submit" value="Guardar"
                                   class="btn btn-primary btn-fill"/>
                            <a href="/visualizar2.php?comida=<?php echo $id_comida; ?>" class="btn btn-info">Ver lo guardado</a>
                            <a href="/visualizar.php" class="btn btn-info">Ver todos</a>
                        </div>

                    </form>

                    <!-- el footer de la página -->
                    <?php include 'parciales/_footer.php';?>
                </div>
            </div>

        </div>
    </div>

</div>
</div>