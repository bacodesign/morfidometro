<div class="header-wrapper">
    <nav class="navbar navbar-default navbar-fixed-top navbar-color-on-scroll navbar-transparent" role="navigation">
        <div class="header">
            <div class="container">
                <h1>
                    <i class="fa fa-cutlery" aria-hidden="true"></i>
                    &nbsp;
                    Morfidómetro
                </h1>
            </div>
        </div>
    </nav>
</div>